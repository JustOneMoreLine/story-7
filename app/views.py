from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'landing.html')

def aboutMe(request):
    return render(request, 'aboutMe.html')

def gallery(request):
    return render(request, 'gallery.html')

def contactMe(request):
    return render(request, 'contactMe.html')

def mySchedule(request):
    scheduleObject = Schedule.getAll()
    schedule = [i.getDate().day for i in scheduleObject]
    cal = calendar.monthcalendar(datetime.date.today().year, datetime.date.today().month)
    calList = [i for i in k for k in cal]
    appointment = []
    for i in calList:
        for j in schedule:
            if i == j:
                appointment.append(j)
    firstday = [i.pop(0) for i in cal]
    w1 = cal[0]
    w2 = cal[1]
    w3 = cal[2]
    w4 = cal[3]
    w5 = cal[4]

    if request.method == "POST":
        form = NewAppointment(request.POST)
        if form.is_valid():
            cleanAppointment = form.cleaned_data["appointment"]
            cleanDate = form.cleaned_data["date"]
            cleanTime = form.cleaned_data["time"]
            t = Schedule(appointment=cleanAppointment, date=cleanDate, time=cleanTime)
            t.save()

            return HttpResponse("<h1>Good Job</h1>")
    elif request.method == "GET":
        form = NewAppointment()
        return render(request, 'mySchedule.html', {"form":form, "cal":cal, "w1":w1, "w2":w2, "w3":w3, "w4":w4, "w5":w5, "firstday":firstday, "appointment": appointment})

def appointments(request):
    appointments = Schedule.objects.all()
    if request.method == "POST":
        form = NewAppointment(request.POST)

        if form.is_valid():
            cleanAppointment = form.cleaned_data["appointment"]
            cleanDate = form.cleaned_data["date"]
            cleanStartTime = form.cleaned_data["start_time"]
            cleanEndTime = form.cleaned_data["end_time"]
            t = Schedule(appointment=cleanAppointment, date=cleanDate, start_time=cleanStartTime, end_time=cleanEndTime)
            t.save()
            return redirect('/appointments/')
    elif request.method == "GET":
        form = NewAppointment()
        return render(request, 'appointments.html', {"form":form, "appointments":appointments})

def delete(request, ids):
    item = Schedule.objects.get(id = ids)
    item.delete()
    form = NewAppointment()
    updatedAppointments = Schedule.objects.all()
    return redirect('/appointments/')