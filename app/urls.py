from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('aboutMe/', views.aboutMe, name='aboutMe'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contactMe, name='contactMe'),
    path('schedule/', views.mySchedule, name='mySchedule'),
    path('appointments/', views.appointments, name='appointments'),
    path('delete/<int:ids>', views.delete, name='delete')
]
