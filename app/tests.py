from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from .models import *
from .forms import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
# Create your tests here.

class UnitTest(TestCase):

    def test_home_template(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertTemplateUsed(resp, 'landing.html')
    
    def test_home_response(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        self.selenium = webdriver.Firefox(firefox_options=options)
        super(FunctionalTest, self).setUp()

    def test_home_button(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        navbar = selenium.find_element_by_id("myNavbar")
        navbarText = selenium.find_elements_by_class_name('nav-link')

        navbarColor = navbar.value_of_css_property('background-color')
        for text in navbarText:
            self.assertEqual(text.value_of_css_property('color'), "rgb(255, 255, 255)")
        self.assertEqual(navbarColor, "rgb(35, 55, 77)")

        selenium.find_element_by_id('option2').click()
        selenium.implicitly_wait(10)

        navbarColor = navbar.value_of_css_property('background-color')
        for text in navbarText:
            self.assertEqual(text.value_of_css_property('color'), "rgb(52, 58, 64)")
        self.assertEqual(navbarColor, "rgb(238, 238, 238)")

        selenium.find_element_by_id('option1').click()
        selenium.implicitly_wait(10)

        navbarColor = navbar.value_of_css_property('background-color')
        for text in navbarText:
            self.assertEqual(text.value_of_css_property('color'), "rgb(255, 255, 255)")
        self.assertEqual(navbarColor, "rgb(35, 55, 77)")

    def test_home_accordion(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        actions = ActionChains(selenium)
        accordions = selenium.find_elements_by_name('accordion')
        accordionText = selenium.find_elements_by_name('accordionText')
        self.assertGreater(len(accordions), 0)
        self.assertEqual(len(accordions), len(accordionText))
        for x in range(len(accordions)-1, -1, -1):
            actions.move_to_element(accordions[x])
            actions.click()
            actions.perform()
            self.assertTrue(accordionText[x].is_displayed())

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()